import React from 'react';
import Link from 'next/link';
import Page from '../components/Page';

class BoardHosting extends React.Component {
  render() {
    return (
      <Page>
        <ul>
          <li>
            Visit this site with a decentralized app browser (like{' '}
            <a
              href="https://metamask.io/"
              target="_blank"
              rel="noopener noreferrer"
            >
              metamask
            </a>)
          </li>
          <li>Create a board using the "Create Board" button above</li>
          <li>Save the contract address</li>
          <li>
            Head to{' '}
            <a
              href="http://code.kevinflo.com/decentralizedboard"
              target="_blank"
              rel="noopener noreferrer"
            >
              code.kevinflo.com/decentralizedboard
            </a>
          </li>
          <li>
            Switch out the contract address for your own and deploy the site
          </li>
        </ul>
      </Page>
    );
  }
}

export default BoardHosting;
