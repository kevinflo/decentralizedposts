import React from 'react';
import MainNavbar from './MainNavbar';

export default props => {
  return (
    <div>
      <MainNavbar />
      {props.children}
    </div>
  );
};
