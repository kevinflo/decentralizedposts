import React from 'react';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button
} from 'reactstrap';

import Link from 'next/link';
import Router from 'next/router';

class MainNavbar extends React.Component {
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <Link href="/" prefetch>
            <NavbarBrand href="/">Decentralized Posts</NavbarBrand>
          </Link>

          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink
                  href="http://code.kevinflo.com/decentralizedposts"
                  target="_blank"
                >
                  Open Source
                </NavLink>
              </NavItem>
              <NavItem>
                <Link href="/boardhosting" prefetch>
                  <NavLink href="/boardhosting">Board Hosting</NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link href="/platform" prefetch>
                  <NavLink href="/platform">Platform</NavLink>
                </Link>
              </NavItem>
              <Link href="/createboard">
                <NavItem>
                  <Button color="primary">Create Board</Button>
                </NavItem>
              </Link>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default MainNavbar;
