import React, { Component } from 'react';
import HasMainNavbar from './HasMainNavbar';
import { Container } from 'reactstrap';
import '../styles/main-styles.scss';

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <HasMainNavbar>
        <Container className="container-main">{this.props.children}</Container>
      </HasMainNavbar>
    );
  }
}

export default Page;
