import React from 'react';
import {
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Tooltip,
  Alert
} from 'reactstrap';

import Link from 'next/link';

import CheckboxRow from './CheckboxRow';
import dposts from '../../ethereum/dposts';
import { ENGINE_METHOD_DIGESTS } from 'constants';
import web3 from '../../ethereum/web3';

/*

  board signature is:

  bytes32 name,
  bool allowPosterEditing,
  bool allowOwnerModeration,
  bool allowBoardTipping,
  bool allowTaxation,
  bool allowPostTipping,
  bool allowComments,
  bool allowCommentTipping,
  uint fee

*/

class BoardCreation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      allowPosterEditing: false,
      allowOwnerModeration: true,
      allowBoardTipping: true,
      allowTaxation: true,
      allowPostTipping: true,
      allowComments: false,
      allowCommentTipping: false,
      fee: 0,
      submitting: false,
      errorMessage: '',
      accounts: [],
      board: {}
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.submitBoard = this.submitBoard.bind(this);
  }

  async submitBoard() {
    this.setState({
      submitting: true,
      errorMessage: ''
    });

    try {
      const accounts = await web3.eth.getAccounts();
      console.log('got accounts3', accounts);
      this.setState({ accounts });

      let feeWei =
        typeof this.state.fee === 'number' && parseFloat(this.state.fee) > 0
          ? web3.utils.toWei(this.state.fee.toString())
          : 0;

      let gasCost = await dposts.methods
        .createBoard(
          web3.utils.asciiToHex(this.state.name),
          this.state.allowPosterEditing,
          this.state.allowOwnerModeration,
          this.state.allowBoardTipping,
          this.state.allowTaxation,
          this.state.allowPostTipping,
          this.state.allowComments,
          this.state.allowCommentTipping,
          feeWei
        )
        .estimateGas({ from: accounts[0] });

      let board = await dposts.methods
        .createBoard(
          web3.utils.asciiToHex(this.state.name),
          this.state.allowPosterEditing,
          this.state.allowOwnerModeration,
          this.state.allowBoardTipping,
          this.state.allowTaxation,
          this.state.allowPostTipping,
          this.state.allowComments,
          this.state.allowCommentTipping,
          feeWei
        )
        .send({ gas: gasCost, from: accounts[0] });

      this.setState({ submitting: false, board: board });
      console.log('board made', board);
    } catch (err) {
      console.log('error yo', err);
      this.setState({ errorMessage: err.message, submitting: false });
    }
  }

  handleInputChange(event) {
    const target = event.target;
    let value;
    switch (target.type) {
      case 'checkbox':
        value = target.checked;
        break;
      case 'number':
        if (target.value) {
          value = parseFloat(target.value);
        }
        break;
      default:
        value = target.value;
        break;
    }

    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <Form className="create-board-form">
        {this.state.errorMessage ? (
          <Alert color="danger">
            {this.state.errorMessage.substring(0, 120)}
          </Alert>
        ) : (
          ''
        )}

        <FormGroup row>
          <Label for="name" sm={4} lg={3}>
            Board Name
          </Label>
          <Col sm={10} lg={9}>
            <Input
              type="text"
              name="name"
              id="name"
              maxLength={32}
              onChange={this.handleInputChange}
              value={this.state.name}
              placeholder="What do you want to call your board?"
            />
          </Col>
        </FormGroup>

        <CheckboxRow
          fieldName="allowPosterEditing"
          handleChange={this.handleInputChange}
          checked={this.state.allowPosterEditing}
          explainer={
            'People ' +
            (this.state.allowPosterEditing ? 'will' : 'will not') +
            ' be allowed to edit their posts after the time of posting'
          }
        />

        <CheckboxRow
          fieldName="allowOwnerModeration"
          handleChange={this.handleInputChange}
          checked={this.state.allowOwnerModeration}
          explainer={
            'The board owner (you) ' +
            (this.state.allowOwnerModeration ? 'will' : 'will not') +
            '  be allowed to change post visibility'
          }
        />

        <CheckboxRow
          fieldName="allowBoardTipping"
          handleChange={this.handleInputChange}
          checked={this.state.allowBoardTipping}
          explainer={
            'People ' +
            (this.state.allowBoardTipping ? 'will' : 'will not') +
            ' be allowed to send tips directly to the board/board owner'
          }
        />

        <CheckboxRow
          fieldName="allowTaxation"
          handleChange={this.handleInputChange}
          checked={this.state.allowTaxation}
          explainer={
            'People ' +
            (this.state.allowTaxation ? 'will' : 'will not') +
            ' be allowed to pay a (voluntary and hard capped) tax to the platform'
          }
        />

        <CheckboxRow
          fieldName="allowPostTipping"
          handleChange={this.handleInputChange}
          checked={this.state.allowPostTipping}
          explainer={
            'People ' +
            (this.state.allowPostTipping ? 'will' : 'will not') +
            ' be allowed to send tips directly to individual posters'
          }
        />

        <CheckboxRow
          fieldName="allowComments"
          handleChange={this.handleInputChange}
          checked={this.state.allowComments}
          explainer={
            'People ' +
            (this.state.allowComments ? 'will' : 'will not') +
            ' be allowed to comment on posts'
          }
        />

        <CheckboxRow
          fieldName="allowCommentTipping"
          handleChange={this.handleInputChange}
          checked={this.state.allowCommentTipping}
          explainer={
            'People ' +
            (this.state.allowCommentTipping ? 'will' : 'will not') +
            ' be allowed to send tips directly to post commenters'
          }
        />

        <FormGroup row>
          <Label for="fee" sm={4} lg={3}>
            Board Fee (ETH)
            <br />
            Leave blank for no fee
          </Label>
          <Col sm={10} lg={9}>
            <Input
              type="number"
              name="fee"
              id="fee"
              onChange={this.handleInputChange}
              placeholder="Mandatory fee required for each post (paid to board owner)"
            />
            {typeof this.state.fee === 'number' &&
            parseFloat(this.state.fee) > 0
              ? '(' +
                web3.utils.toWei(this.state.fee.toString()) +
                ' wei per post)'
              : ''}
          </Col>
        </FormGroup>

        <FormGroup check row>
          <Col xs={12}>
            <Button
              disabled={this.state.submitting}
              color="primary"
              onClick={this.submitBoard}
              hidden={Object.keys(this.state.board).length != 0}
            >
              {this.state.submitting ? 'Submitting...' : 'Submit'}
            </Button>
            {this.state.submitting ? (
              <React.Fragment>
                <img src="https://loading.io/spinners/bars/index.progress-bar-facebook-loader.svg" />
                <Alert color="danger">
                  DO NOT CLOSE OUT OF THIS TILL SUBMISSION IS COMPLETE AND YOU
                  HAVE SAVED YOUR DEPLOYED CONTRACT ADDRESS. May take a few
                  minutes...
                </Alert>
              </React.Fragment>
            ) : (
              ''
            )}
            {Object.keys(this.state.board).length ? (
              <Alert color="success">
                Success! Board contract id: <br />
                {this.state.board.events.BoardCreated.returnValues._board}{' '}
                <br />
                COPY THAT AND SAVE IT and then head to{' '}
                <Link href="/boardhosting">
                  <a>Board Hosting</a>
                </Link>{' '}
                for info on what to do next
              </Alert>
            ) : (
              ''
            )}
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default BoardCreation;
