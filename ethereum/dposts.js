import web3 from './web3';
import Dposts from './build/Dposts.json';
const addresses = require('../addresses.json');

const instance = new web3.eth.Contract(
  JSON.parse(Dposts.interface),
  addresses.DPOSTS.MAINNET
);

export default instance;
