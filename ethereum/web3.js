import Web3 from 'web3';
const INFURA_RINKEBY = require('../secrets.json').INFURA2.RINKEBY;
const INFURA_MAINNET = require('../secrets.json').INFURA2.MAINNET;

let web3;
if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  web3 = new Web3(window.web3.currentProvider);
  web3.isServer = false;
} else {
  const provider = new Web3.providers.HttpProvider(INFURA_MAINNET);
  web3 = new Web3(provider);
  web3.isServer = true;
}

export default web3;
